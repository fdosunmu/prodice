package P10_26;

import javax.swing.*;

/**
 * Created by OlufunmilayoDosunmu on 11/1/15.
 */
public class OlympicRingsViewer {
    public static void main(String[] args){
        JFrame frame = new JFrame();

        frame.setSize(600, 600);
        frame.setTitle("Olympic Rings");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new OlympicRingsComponent();
        frame.add(component);

        frame.setVisible(true);

    }
}
