package P10_26;

import javax.swing.*;
import java.awt.*;

/**
 * Write a program that displays the Olympic rings. Color the rings in the Olympic
 colors. Provide a method drawRing that draws a ring of a given position and color.
 * Created by OlufunmilayoDosunmu on 11/1/15.
 */
public class OlympicRingsComponent extends JComponent {
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;

    public void drawRing(Graphics ring, int x, int y, int diameter, Color pick) {
        ring.setColor(pick);
        ring.drawOval(x, y, diameter, diameter);

    }

    public void paintComponent(Graphics g) {
        drawRing(g, 100, 10, 100, Color.BLUE);
        drawRing(g, 180, 10, 100, Color.BLACK);
        drawRing(g, 260, 10, 100, Color.RED);
        drawRing(g, 150, 90, 100, Color.YELLOW);
        drawRing(g, 230, 90, 100, Color.GREEN);
    }
}



