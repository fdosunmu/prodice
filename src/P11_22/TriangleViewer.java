package P11_22;

import javax.swing.*;

/**
 * Created by OlufunmilayoDosunmu on 11/1/15.
 */
public class TriangleViewer extends JFrame {
    public static void main(String[] args) { JFrame frame = new JFrame();

        frame.setSize(600, 600);
        frame.setTitle("Triangle");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new Triangle();
        frame.add(component);

        frame.setVisible(true);


    }

}

