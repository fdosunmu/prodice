package P11_22;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**Write a program that allows the user to specify a triangle with three mouse presses.
 After the first mouse press, draw a small dot. After the second mouse press, draw a
 line joining the first two points. After the third mouse press, draw the entire triangle.
 The fourth mouse press erases the old triangle and starts a new one.

 * Created by OlufunmilayoDosunmu on 11/1/15.
 */
public class Triangle extends JComponent {
    //private JPanel mPanel;
    private Point mOne, mTwo, mThree;


    //private static final int FRAME_WIDTH = 500;
   // private static final int FRAME_HEIGHT = 500;

    //public static void main(String[] args) {
       // JFrame frame = new JFrame("Draw Triangle");
      //  frame.setContentPane(new Triangle().mPanel);
       // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       // frame.pack();
       // frame.setSize(400, 400);
       // frame.setVisible(true);
//    }

   public Triangle() {

       class MouseClickedListener implements MouseListener {
           public void mouseClicked(MouseEvent e) {
               mOne = e.getPoint();
               mTwo = e.getPoint();
               mThree = e.getPoint();
           }

           public void mouseEntered(MouseEvent e) {
           }

           public void mouseExited(MouseEvent e) {
           }

           public void mousePressed(MouseEvent e) {
           }

           public void mouseReleased(MouseEvent e) {
           }

       }}

    public void paintComponent(Graphics g) {
        g.drawLine(mOne.x, mOne.y, mTwo.x, mTwo.y);
        g.drawLine(mTwo.x, mTwo.y, mThree.x, mThree.y);
        g.drawLine(mThree.x, mThree.y, mOne.x, mOne.y);
    }
}