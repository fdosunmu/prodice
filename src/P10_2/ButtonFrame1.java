package P10_2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 •P10.2 Enhance the ButtonViewer1 program in Section 10.2.1 so that it prints a message
 “I was clicked n times!” whenever the button is clicked. The value n should be incremented with each click.
 * Created by OlufunmilayoDosunmu on 11/1/15.
 */
public class ButtonFrame1  extends JFrame {
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 60;

    public ButtonFrame1() {
        createComponents();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

    }

    private void createComponents() {
        JButton button = new JButton("Click me");
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel);

        class ClickListener implements ActionListener{
            private int clicks;
            public void actionPerformed(ActionEvent event){
                clicks += 1;
                System.out.println("I was clicked " + clicks + " times.");
            }
        }
        ActionListener listener = new ClickListener();
        button.addActionListener(listener);
    }

}