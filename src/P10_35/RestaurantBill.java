package P10_35;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**

Write a graphical application that produces a restaurant bill.
 Provide buttons for ten popular dishes or drink items. (You
 decide on the items and their prices.) Provide text fields for
 entering less popular items and prices. In a text area, show the
 bill, including tax and a suggested tip.

 * Created by OlufunmilayoDosunmu on 11/1/15.
 **/

public class RestaurantBill extends JFrame {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 400;
    private JLabel billLabel;

    public RestaurantBill(){
        createComponents();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

    }

    private void createComponents() {
        billLabel = new JLabel("0.00");
        mealButtons();

    }
    private void mealButtons(){
        JButton lasagna = new JButton("Lasagna");
        JButton salmon = new JButton("Salmon");
        JButton jollofRice = new JButton("Jollof Rice");
        JButton steak = new JButton("Steak");
        JButton chicken = new JButton("Chicken");
        JButton salad = new JButton("Salad");
        JButton iceCream = new JButton("Ice Cream");
        JButton coke = new JButton("Coke");
        JButton sprite = new JButton("Sprite");
        JButton cocktail = new JButton("Cocktail");
        JButton completePurchase = new JButton("Complete Purchase");

        JPanel mealPanel = new JPanel();
        mealPanel.add(lasagna);
        mealPanel.add(salmon);
        mealPanel.add(jollofRice);
        mealPanel.add(steak);
        mealPanel.add(chicken);
        mealPanel.add(salad);
        mealPanel.add(iceCream);
        mealPanel.add(coke);
        mealPanel.add(sprite);
        mealPanel.add(cocktail);
        mealPanel.add(completePurchase);
        add(mealPanel);



        class AddItemListener implements ActionListener{
            private double price;

            public AddItemListener(double price){
                this.price = price;
            }
            public void actionPerformed(ActionEvent e){
                double currentBill = Double.valueOf(billLabel.getText()) + price;
                billLabel.setText(String.format("%.4f", currentBill));
            }


        }
        lasagna.addActionListener(new AddItemListener(15.95));
        salmon.addActionListener(new AddItemListener(22.50));
        jollofRice.addActionListener(new AddItemListener(8.95));
        steak.addActionListener(new AddItemListener(30.95));
        chicken.addActionListener(new AddItemListener(13.50));
        salad.addActionListener(new AddItemListener(5.95));
        iceCream.addActionListener(new AddItemListener(4.50));
        coke.addActionListener(new AddItemListener(2.50));
        sprite.addActionListener(new AddItemListener(2.50));
        cocktail.addActionListener(new AddItemListener(12.00));
        completePurchase.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double tax = Double.valueOf(billLabel.getText()) * 1.15;
                double tip = Double.valueOf(billLabel.getText()) * 1.20 + tax;
                JOptionPane.showMessageDialog(null, String.format("Your bill is: %s", billLabel.getText(), "With Tax: %s", tax, "Total with suggested 20% tip is: %s", tip, "Purchase Completed!", JOptionPane.INFORMATION_MESSAGE));
                billLabel.setText("0.00");
            }
            });
        }
    }

