#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################
R10.5 Events
What is an event object? An event source? An event listener?

Event objects objects are instances of classes that you must provide.

An event listener belongs to a class created by the application programmer. its methods describe the actions to
be taken when an event occurs.

Event sources report on events. When an event occurs, the event source notifies all event listeners.

R10.6 actionPerformed
Who calls the actionPerformed method of an event listener? When does the call to the
actionPerformed method occur?

It is the programmers job to supply a  class whose actionPerformed method contains the instructions that the
programmer wants executed whenever the button is clicked.

R10.11 Inner class and event-listeners
Why are we using inner classes for event listeners? If Java did not have inner classes,
could we still implement event listeners? How?

Methods of inner classes can access instance variables and methods of the surrounding class.
Some programmers bypass the event list ener classes and turn a frame into a listener.

R10.14 Object hierarchies
Name a method that is declared in JTextArea, a method that JTextArea inherits from
JTextComponent, and a method that JTextArea inherits from JComponent.

append method is declared in JTextArea
addInputMethodListener is inherited by JTextArea from JTextComponent
addNotify is inherited from JComponent by JTextArea

R10.22 Graphic methods
How would you modify the drawItalianFlag method in How To 10.1 to draw any
flag with a white vertical stripe in the middle and two arbitrary colors to the left and
right?

I would use g.setColor to change the color the stripes

R11.2 Layout managers
What is the advantage of a layout manager over telling the container “place this component at position (x, y)”?

It  gives independence to the GUI platform... makes it more portable

R11.11 ButtonGroup
Why do you need a button group for radio buttons but not for check boxes?

There is only able to be one and only one selection for radio buttons, there for a set of radio buttons must be grouped.
In contrast, a user can check one or more check boxes.


R11.19 Types of Events
What is the difference between an ActionEvent and a MouseEvent?

Action event executed a button that was clicked. Whereas A Mouse event tracks the movement of the mouse,
including when mouse is clicked, pressed, etc...

R11.20 Events
What information does an action event object carry? What additional information
does a mouse event object carry? Hint: Check the API documentation.

An action event object carries information about the event occurred.
A mouse event object carries information about the mouse event.
it also contains methods addMouseMotionListener and MouseListener methods.

R11.21 ActionListener versus MouseListener
Why does the ActionListener interface have only one method, whereas the Mouse­­
Listener has five methods?

A mouse can have many events take place, ie: pressing and dragging, clicking etc
so it needs methods to account for these actions

In actionlistener the user is interacting with buttons or textfield, which
is one event.




